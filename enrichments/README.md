This agent provides CMA with a way to bootstrap the register with informaiton that cannot be 
cross-referneced automatically - For example, by providing a mapping from DAB SID values to 
Ofcom License numbers we can identify the same broadcast service in different registers and contexts.
This file is the place where we can essentially make "Same-As" assertions that enable RR to know
that the broadcast-service with SID "CA8A" is the "Same-As" the station with License Number "CR000199BA/4"
and prevents us creating duplicate records for these cases
