#!/usr/bin/env groovy
import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('net.sf.opencsv:opencsv:2.3')
@Grab(group='org.jsoup', module='jsoup', version='1.6.2')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')

import org.jsoup.Jsoup
import org.jsoup.nodes.*
import org.jsoup.select.*
import static groovy.json.JsonOutput.*
import groovy.json.JsonOutput
import org.ini4j.*;
import groovy.json.JsonOutput;
import java.util.regex.*;
import groovy.json.JsonSlurper
import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.CSVWriter
import java.security.MessageDigest
import static groovyx.net.http.HttpBuilder.configure
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer

if ( args.length == 0 )
  System.exit(1);

if ( args[0] == null )
  System.exit(1);
  
String required_config = args[0]

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.agents/radiodns'));
String keycloak_url = ini.get(required_config, 'url', String.class);
String keycloak_realm = ini.get(required_config, 'realm', String.class);
String keycloak_user = ini.get(required_config, 'username', String.class);
String keycloak_pass = ini.get(required_config, 'password', String.class);
String target_url = ini.get(required_config, 'rr_target_url', String.class) + '/admin/actions/assertData';

println("Running agent with config ${required_config} - ${target_url}");

if ( target_url == null )
  System.exit(1);

Map config = getConfig(required_config)
long runtime = System.currentTimeMillis();

def keycloak = configure {
  request.uri = keycloak_url
}

String session_jwt = login(keycloak, 'selfservice',keycloak_realm,keycloak_user,keycloak_pass);

println("JWT will be ${session_jwt}");

config.lastRunStr=new Date(runtime).toString()
config.lastRun=runtime


// checkForServiceChanges(config, 'https://www.radiodns.uk/services.json', session_jwt, target_url);
checkForMultiplexChanges(config, 'https://www.radiodns.uk/multiplexes.json', session_jwt, target_url, { record -> record.licence_number.startsWith('SSDAB') } );

writeConfig(config, required_config);

System.exit(0);

private checkForMultiplexChanges(Map config, String url, String jwt, String target_url, Closure condition) {
  println("Reading radiodns multiplex....");
  def multiplexes = new JsonSlurper().parseText(new java.net.URL(url).text)
  println("Processing....");

  if ( config.multiplexRegister == null ) 
    config.multiplexRegister = []

  multiplexes.each { mp ->

    // If we were given a row selection condition apply it here
    if ( ( condition == null ) ||
         ( condition(mp) == true ) ) {
      if ( config.multiplexRegister.containsKey(mp.'@id') ) {
        if ( mp.updated_at > config.multiplexRegister[mp.'@id'].updated_at ) {
          processMultiplexUpdate(mp, config, jwt, target_url)
        }
        else {
            println("No change: ${mp.'@id'}");
        }
      }
      else {
        processMultiplexUpdate(mp, config, jwt, target_url)
      }
    }
    else {
      println("Skipping ${mp.'@id'} - did not pass condition")
    }
  }
}

private checkForServiceChanges(Map config, String url, String jwt, String target_url) {

  println("Reading radiodns service....");
  def services = new JsonSlurper().parseText(new java.net.URL(url).text)
  println("Processing....");
  services.each { svc ->
    // For now - we only process DAB
    if ( svc.'@id'.contains('/dab/') ) {
      if ( config.serviceRegister.containsKey(svc.'@id') ) {
        if ( svc.updated_at > config.serviceRegister[svc.'@id'].updated_at ) {
          processServiceUpdate(svc, config, jwt, target_url)
        }
        else {
          println("No change: ${svc.'@id'}");
        }
      }
      else {
        processServiceUpdate(svc, config, jwt, target_url)
      }
    }
    else {
      println("  SKIP: ${svc.'@id'} - not DAB");
    }
  }

}

private processMultiplexUpdate(Map mp, Map config, String jwt, String target_url) {
  String record_text = new java.net.URL("${mp.'@id'}.json".toString()).text;
  try {
    Map new_entry = new JsonSlurper().parseText(record_text)
    postMultiplexUpdate(new_entry, config, jwt, target_url)
    config.multiplexRegister[new_entry.'@id'] = new_entry;
  }
  catch ( Exception e ) {
    e.printStackTrace()
    println(record_text)
  }
  Thread.sleep(500);
}

private processServiceUpdate(Map serviceListEntry, Map config, String jwt, String target_url) {
  String record_text = new java.net.URL("${serviceListEntry.'@id'}.json".toString()).text;
  try {
    Map new_entry = new JsonSlurper().parseText(record_text)
    postServiceUpdate(new_entry, config, jwt, target_url)
    config.serviceRegister[new_entry.'@id'] = new_entry;
  }
  catch ( Exception e ) {
    e.printStackTrace()
    println(record_text)
  }
  Thread.sleep(500);
}

private void postMultiplexUpdate(Map record, Map config, String jwt, String target_url) {
  println("  POST MULTIPLEX UPDATE ${record.eid} ${record.licence_number} ${record.name} ${record.area} ${record.block}");
  def update = [
    type: 'multiplex',
    options:[
      upsert:true
    ],
    match:[
      ead:record.eid
      licenseNumber:record.licence_number
    ],
    authorityData:[
      ead:record.eid,
      licenseNumber:record.licence_number
    ],
    supplementaryData:[
      name: record.name
    ]
  ]

  securePost(target_url, // 'https://uat.semweb.co/dabdata/admin/actions/assertData',
             jwt,
             update);
}

private void postServiceUpdate(Map record, Map config, String jwt, String target_url) {
  String[] parsed_fqdn_1 = record.'@id'.split('\\:');
  String[] parsed_fqdn_2 = parsed_fqdn_1[1].split('\\.')  // country.eid.sid.scids

  println("  POST UPDATE ${parsed_fqdn_1[0]} - p2:${parsed_fqdn_2} / \"${record.long_name ?: record.medium_name ?: record.short_name}\" to ${target_url}");
  if ( parsed_fqdn_1[0] == 'dab' ) {
    println("Attempt to update or insert broadcast service with sid ${parsed_fqdn_2[2]}");
    def update = [
      type: 'broadcastService',
      options:[
        upsert:true
      ],
      match:[
        fqdn:record.'@id',
        sid:parsed_fqdn_2[2].toUpperCase()
      ],
      authorityData:[
        fqdn:record.'@id',
        authority:record.authority,
        name:record.long_name ?: record.medium_name ?: record.short_name,
      ],
      supplementaryData:[
        description: record.long_description ?: record.short_description,
        logo32: record.logos?.'32x32',
        logo128: record.logos?.'128x128',
      ]
    ]

    // println("Attempt update: ${update}");

    securePost(target_url, // 'https://uat.semweb.co/dabdata/admin/actions/assertData',
               jwt,
               update);
  }
  else {
    println("Parsed fqdn != dab : ${parsed_fqdn_1[0]}");
  }
}

private void securePost(String url, String bearer, Map body) {
  def builder = configure{
    request.uri = url
    request.headers['accept']='application/json'
    request.headers['Authorization'] = "Bearer $bearer".toString()
    request.contentType='application/json'
    request.body = body
  }

  def post_result = builder.post {
    response.when(200) { FromServer fs, Object rbody ->
      println("RESPONSE:: ${body}");
    }

    response.failure { FromServer fs, Object rbody ->
      println("Problem ${rbody} ${fs} ${fs.getStatusCode()}");
    }
  }
}


private Map getConfig(String required_config) {
  def jsonSlurper = new JsonSlurper()
  Map mappings = [:]
  Map result = null;

  try {
    File params_file = new File("./${required_config}-DNSRadio.json".toString());
    if ( params_file.exists() ) {
      FileReader reader = new FileReader("./${required_config}-DNSRadio.json".toString())
      result = jsonSlurper.parse(reader);
    }
  } catch (Exception e) {
    e.printStackTrace()
  }

  if ( result == null ) {
    result = [
      serviceRegister:[:],
      multiplexRegister:[:],
      runLog:[]
    ]
  }

  return result;
}

private void writeConfig(config, required_config) {
  try {
    File params_file = new File("./${required_config}-DNSRadio.json".toString())
    if ( params_file.exists() ) {
      println("Backing up old config file");
      params_file.renameTo("./${required_config}-DNSRadio.json.bak".toString())
    }

    params_file << JsonOutput.prettyPrint(JsonOutput.toJson(config))
  }
  catch ( Exception e ) {
    e.printStackTrace();
  }
}

public String login(HttpBuilder keycloak, String client, String realm, String username, String password) {

  String result = null;
  println("Attempt login using client ${client}, realm ${realm}, user ${username},...");

  def http_res = keycloak.post {
    request.uri.path = "/auth/realms/${realm}/protocol/openid-connect/token".toString()
    request.contentType='application/x-www-form-urlencoded'
    request.body=[
      client_id:client,
      username:username,
      password:password,
      grant_type:'password'
    ]

    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
      result = body.access_token
    }

    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }

  this.jwt = result;
  return result;
}

