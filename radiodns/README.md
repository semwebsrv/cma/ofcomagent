Service list is at

https://www.radiodns.uk/services.json. Each entry contains an @id that points to a service file eg 
https://www.radiodns.uk/services/dab/ce1/c1c5/c5e7/0 - adding .json gives https://www.radiodns.uk/services/dab/ce1/c1c5/c5e7/0.json


See RadioDNS techinal spec at https://radiodns.org/wp-content/uploads/2014/02/RDNS01-1.0.0.pdf


watch curl -s https://uat.semweb.co/dabdata/

Interesting test case: https://www.radiodns.uk/services/dab/ce1/c188/c3a8/0 / C3A8
