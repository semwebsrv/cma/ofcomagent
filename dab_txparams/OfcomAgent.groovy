#!/usr/bin/env groovy
import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('net.sf.opencsv:opencsv:2.3')


import static groovy.json.JsonOutput.*
import groovy.json.JsonOutput
import org.ini4j.*;
import groovy.json.JsonOutput;
import java.util.regex.*;
import groovy.json.JsonSlurper
import au.com.bytecode.opencsv.CSVReader

if ( args.length == 0 ) {
  println("Usage : OfcomAgent.groovy DATA_FILE");
  System.exit(1);
}

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.agents/ofcom_dab'));
// String url = ini.get('ofcom', 'daburl', String.class);
// println "process url ${url}"
println("Process file ${args[0]}");

Map config = getConfig()
long runtime = System.currentTimeMillis();

config.lastRunStr=new Date(runtime).toString()
config.lastRun=runtime

File datafile = new File(args[0])
if ( datafile.exists() ) {
  process(datafile, config, runtime);
}
else {
  println("File ${args[0]} appears not to exist");
}

writeConfig(config);

System.exit(0);


// Read through the file, generate a JSON representaiton of each line, checksum it and
// take action if there is a difference to the last run
private process(File input_file, Map state, long runtime) {
  char del = ','
  char quote = '"'
  def r = new CSVReader(new InputStreamReader(input_file.newInputStream(), java.nio.charset.Charset.forName('UTF-8')), del, quote)

  String[] nl = r.readNext()
  if ( nl != null ) {
    // Capture header row
    String[] header_row = nl

    // Attempt to read first line of data
    nl = r.readNext()
  
    checkDataDir();

    def stats = [
      num_ensembles:0,
      lines_processed:0,
      additional_transmitters:0
    ]

    Map<String,Object> etData = [:]
  
    int rowctr = 0
    // While we have data
    while (nl != null) {
      readEnsembleTransmitterLine(nl, state, runtime, etData, stats);

      // get next row
      rowctr++
      nl = r.readNext()
    }
  }
}

public void readEnsembleTransmitterLine(String[] dataline, Map state, long runtime, Map<String,Object> etData, Map stats) {

  String ensembleId = dataline[4]

  // The datafile contains multiple lines for each ensemble - one per transmitter we build up a picture here
  // by taking the majority of details from the first line, then enriching with subsequent lines (and also auditing the data)
  if ( etData[ensembleId] == null ) {
    println("New ensemble: ${ensembleId}");
    etData[ensembleId] = [
      dateFromParamsFile:dataline[0],
      ensembleLabel:dataline[1],
      license: dataline[2],
      ensembleArea: dataline[3],
      ensembleId: ensembleId,
      transmitters: [ [ transmitterId: dataline[5] ] ],  // Transmitters is an array of maps- we will add to this array for each line for this ensemble in the file
    ]

    // 0 based, Service data starts at column 11 in blocks of three so
    // 11: Service label 1, 12: Sid 1(HEX), 13:LSN 1(HEX), 14:Service Label 2.....
    // there is space for 32 services, with service 32 LSN being column 106
    // Data services start at column 107 and only have 2 properties - label and SID with space for 15 data services
    // last data service SID is in column 136
    
    // Transmitter info starts at column 137 with NGR
    stats.num_ensembles++;
  }
  else {
    println("Add transmitter to existing ensemble: ${ensembleId}");
    // Columns here are 0 based
    //
    // Between different transmitter rows for the same ensemble, columns 0(date),5(Transmitter Area),6(Site),10(TII Sub Id (HEX)),
    // and then the remaning transmitter columns such as NGR (columns 137 onwards) are transmitter specific, the remaining should
    // be consistent over all transmitters for that ensemble
    etData[ensembleId].transmitters.add([transmitterId: dataline[5]]);
    stats.additional_transmitters++;
  }
  stats.lines_processed++;
}

private void checkDataDir() {
  File datadir = new File('./data')
  if ( !datadir.exists() ) {
    datadir.mkdir()
  }
}


private Map getConfig() {
  def jsonSlurper = new JsonSlurper()
  Map mappings = [:]
  Map result = null;

  try {
    File params_file = new File('./DABParams.json');
    if ( params_file.exists() ) {
      FileReader reader = new FileReader("./DABParams.json")
      result = jsonSlurper.parse(reader);
    }
  } catch (Exception e) {
    e.printStackTrace()
  }

  if ( result == null ) {
    result = [
      multiplexeRegister:[:],
      runLog:[]
    ]
  }

  return result;
}

private void writeConfig(config) {
  try {
    File params_file = new File("./DABParams.json")
    if ( params_file.exists() ) {
      params_file.renameTo('./DABParams.json.bak')
    }

    params_file << JsonOutput.prettyPrint(JsonOutput.toJson(config))
  }
  catch ( Exception e ) {
    e.printStackTrace();
  }
}
